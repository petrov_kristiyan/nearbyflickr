package com.students.nearbyflickr;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;

import com.google.android.gms.location.LocationRequest;


public class SettingsActivity extends PreferenceActivity {
    static Intent returnIntent ;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preferences);
        returnIntent = new Intent();
        ListPreference priorityLocation = (ListPreference) findPreference("prefLocationPriority");
        priorityLocation.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
                                                           @Override
                                                           public boolean onPreferenceChange(Preference preference, Object newValue) {
                                                               returnIntent.putExtra("prefLocationPriority", true);
                                                               setResult(1, returnIntent);
                                                               return changePreferences(newValue);
                                                           }
                                                       }
        );

        ListPreference prefUpdateFrequency = (ListPreference)findPreference("prefUpdateFrequency");
        prefUpdateFrequency.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                returnIntent.putExtra("prefUpdateFrequency", true);
                setResult(1, returnIntent);
                return true;
            }
        });

        ListPreference prefAccuracy = (ListPreference)findPreference("prefAccuracy");
        prefAccuracy.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                returnIntent.putExtra("prefAccuracy", true);
                setResult(1, returnIntent);
                return true;
            }
        });
        ListPreference prefPhoto = (ListPreference)findPreference("prefPhotoNumber");
        prefPhoto.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                returnIntent.putExtra("prefPhotoNumber", true);
                setResult(1, returnIntent);
                return true;
            }
        });

    }

    public boolean changePreferences(Object newValue) {
        String message = "";
        if (newValue.equals(""+LocationRequest.PRIORITY_HIGH_ACCURACY)) {
            message = "Are you sure you want to change the priority of the location update to HIGH?" +
                    "\nThe update frequency of the location shall be faster. Be aware, it will consume a lot of battery!!";
        } else {
            if(newValue.equals(""+LocationRequest.PRIORITY_LOW_POWER)) {
                message = "Are you sure you want to change the priority of the location update to LOW?" +
                        "\nThe update frequency of the location shall be slower but battery efficient.";
            }
            else{
                message ="This option is the best compromise between locating the device and battery life drain.\n Good choice!!";
            }
        }
        new AlertDialog.Builder(SettingsActivity.this)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle("Set Location Priority")
                .setMessage(message)
                .setPositiveButton("OK", null)
                .show();
        return true;
    }

}