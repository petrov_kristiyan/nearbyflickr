package com.students.nearbyflickr.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.students.nearbyflickr.Main_Activity;
import com.students.nearbyflickr.R;
import com.students.nearbyflickr.SingleImageActivity;
import com.students.nearbyflickr.flickr.FlickrImage;

import java.util.ArrayList;

public class GridViewAdapter extends ArrayAdapter {
    private Context context;
    private int layoutResourceId;
    private ArrayList data = new ArrayList();

    public GridViewAdapter(Context context, int layoutResourceId, ArrayList data) {
        super(context, layoutResourceId, data);
        this.layoutResourceId = layoutResourceId;
        this.context = context;
        this.data = data;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View row = convertView;
        final ViewHolder holder;
        if (row == null) {
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);
            holder = new ViewHolder();
            holder.image = (ImageView) row.findViewById(R.id.image);
            row.setTag(holder);
        } else {
            holder = (ViewHolder) row.getTag();
        }

        FlickrImage item = (FlickrImage) data.get(position);
        final ProgressBar progressBar = (ProgressBar) ((View) parent.getParent()).findViewById(R.id.progressBar);
        if (Main_Activity.getCount() >= 1) {
            Main_Activity.setCount(0);
            Main_Activity.setPagingEnabled(true);
        }

        Picasso.with(context).load(item.getThumbURL()).into(holder.image, new Callback.EmptyCallback() {
            @Override
            public void onSuccess() {
                int count = Main_Activity.getCount();
                count ++;
                Main_Activity.setCount(count);    //increase counter of the number of photos downloaded
                progressBar.setVisibility(View.GONE);//hide loading bar
            }

            @Override
            public void onError() {
                progressBar.setVisibility(View.VISIBLE);
            }
        });
        final String url = item.getLargeURL();
        final String id = item.getId();
        holder.image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {//on click on image start the activity with image in full screen
                Intent intent = new Intent(context, SingleImageActivity.class);
                intent.putExtra("URL", url);
                intent.putExtra("ID", id);
                context.startActivity(intent);
            }
        });
        return row;
    }


    static class ViewHolder {
        ImageView image;
    }

}