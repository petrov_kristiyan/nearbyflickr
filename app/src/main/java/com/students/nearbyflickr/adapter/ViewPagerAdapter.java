package com.students.nearbyflickr.adapter;

import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.view.ViewGroup;

import com.google.android.gms.location.LocationClient;
import com.students.nearbyflickr.flickr.FlickrManager;
import com.students.nearbyflickr.fragment.GalleryFragment;
import com.students.nearbyflickr.fragment.TutorialFragment;

/**
 * Created by kristiyan on 29/11/14.
 */
public class ViewPagerAdapter extends FragmentStatePagerAdapter {
    private static final int ITEMS = Integer.MAX_VALUE; //number of pages
    private int page;   //currrent page
    private FragmentManager fragmentManager;
    private LocationClient locationClient;

    public ViewPagerAdapter(FragmentManager fragmentManager, LocationClient locationClient, int page) {
        super(fragmentManager);
        this.page = page;
        this.locationClient = locationClient;
        this.fragmentManager = fragmentManager;
    }

    @Override
    public int getCount() {
        return ITEMS;
    }

    @Override
    public Fragment getItem(int position) {
        if (position == 0)
            return new TutorialFragment();//tutorial first fragment
        else {
            GalleryFragment galleryFragment = new GalleryFragment();
            Location loc = locationClient.getLastLocation();
            String url = FlickrManager.createURL(loc.getLatitude(), loc.getLongitude(), position);
            Bundle bundle = new Bundle();
            bundle.putString("URL", url);
            bundle.putInt("PAGE", position);
            galleryFragment.setArguments(bundle);
            return galleryFragment; //create gallery fragment
        }

    }

    public void setPage(int page) {
        this.page = page;
    }

    @Override
    public int getItemPosition(Object item) {//called from adapter on notifyDataChange
        return POSITION_NONE;
    }
}
