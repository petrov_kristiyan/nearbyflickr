package com.students.nearbyflickr.intentService;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;

public class Download_JSON extends IntentService {

    public static final String URL_PHOTOS_GALLERY = "URL_PHOTOS";
    public static final String IMG_GEO_LOCATION_URL = "IMG_GEO_LOCATION_URL";
    public static final String NOTIFICATION_GALLERY = "com.students.nerbyflickr.service.gallery";
    public static final String NOTIFICATION_PHOTO_GEO_LOCATION = "com.students.nearbyflickr.service.photoLocation";


    public Download_JSON() {
        super("Download_JSON");
    }

    public static boolean isOnline(Context ctx) {
        ConnectivityManager cm = (ConnectivityManager) ctx.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo network = cm.getActiveNetworkInfo();
        if (network != null && network.isConnectedOrConnecting()) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (isOnline(getApplicationContext())) {
            Bundle args = intent.getExtras();
            if (args != null) {
                String json = "";
                if (args.containsKey(IMG_GEO_LOCATION_URL)) {   //request from Map activity
                    json = getJsonResponse(intent.getStringExtra(IMG_GEO_LOCATION_URL));
                    publishResults(args.getInt("PAGE"),json, NOTIFICATION_PHOTO_GEO_LOCATION);
                } else {    //request from GalleryFragment
                    json = getJsonResponse(intent.getStringExtra(URL_PHOTOS_GALLERY));
                    publishResults(args.getInt("PAGE"),json, NOTIFICATION_GALLERY);
                }

            }

        }
    }

    private String getJsonResponse(String urlS) {
        StringBuilder response = new StringBuilder();
        try {
            BufferedReader reader = null;
            String data = "";
            // Defined URL  where to send data
            URL url = new URL(urlS);
            URLConnection conn = url.openConnection();
            conn.setDoOutput(true);
            OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
            wr.write(data);
            wr.flush();

            // Get the server response

            reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));

            String line = null;

            // Read Server Response
            while ((line = reader.readLine()) != null) {
                // Append server response in string
                response.append(line + "");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return response.toString();
    }

    private void publishResults(int page,String json, String notification) {
        Intent intent = new Intent(notification);
        intent.putExtra("JSON", json);
        intent.putExtra("PAGE",page);
        sendBroadcast(intent);
    }

}
