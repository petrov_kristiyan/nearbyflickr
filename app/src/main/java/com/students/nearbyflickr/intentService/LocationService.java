package com.students.nearbyflickr.intentService;

import android.app.IntentService;
import android.content.Intent;
import android.location.Location;

import com.google.android.gms.location.LocationClient;

public class LocationService extends IntentService {

    public static final String LOCATION = "com.students.nerbyflickr.service.location";
    private static final String LATITUDE = "LATITUDE";
    private static final String LONGITUDE = "LONGITUDE";

    public LocationService() {
        super("LocationService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Location location = intent.getParcelableExtra(LocationClient.KEY_LOCATION_CHANGED);
        if (location != null) {
            Intent response = new Intent(LOCATION);
            response.putExtra(LATITUDE, location.getLatitude());
            response.putExtra(LONGITUDE, location.getLongitude());
            sendBroadcast(response);
        }
    }

}
