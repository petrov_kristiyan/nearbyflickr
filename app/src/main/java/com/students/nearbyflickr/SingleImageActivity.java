package com.students.nearbyflickr;

import android.app.Activity;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.NotificationCompat;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.io.File;
import java.io.FileOutputStream;


public class SingleImageActivity extends Activity {
    private String url;
    private String name = "";
    private ProgressBar progressBar;

    private Target target = new Target() {
        @Override
        public void onBitmapLoaded(final Bitmap bitmap, Picasso.LoadedFrom from) {
            File directory = new File(Environment.getExternalStorageDirectory().getPath() + "/nearbyFlickr/");
            directory.mkdirs();
            File file = new File(directory.toString() +"/"+ name + ".jpeg");
            try {
                file.createNewFile();
                FileOutputStream ostream = new FileOutputStream(file);
                bitmap.compress(Bitmap.CompressFormat.JPEG, 75, ostream);
                ostream.flush();
                ostream.close();
                NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
                NotificationCompat.Builder noti = new NotificationCompat.Builder(SingleImageActivity.this);
                noti.setContentTitle("Photo Saved");
                noti.setContentText(directory.getCanonicalPath());
                noti.setSmallIcon(R.drawable.ic_menu_gallery);

                Intent shareIntent = new Intent(Intent.ACTION_VIEW);
                shareIntent.setDataAndType(Uri.fromFile(file),"image/*");
                PendingIntent pendingIntent = PendingIntent.getActivity(SingleImageActivity.this,1, shareIntent, 0);
                noti.setContentIntent(pendingIntent);
                Bitmap largeIcon = getLargeIcon(bitmap);
                if (largeIcon != null) {
                    noti.setLargeIcon(largeIcon);
                }

                notificationManager.notify(1234, noti.build());
                Toast.makeText(SingleImageActivity.this, "Downloaded", Toast.LENGTH_SHORT).show();
            } catch (Exception e) {
                Toast.makeText(SingleImageActivity.this, "Error download photo", Toast.LENGTH_SHORT).show();
                e.printStackTrace();
            }
        }

        public Bitmap getLargeIcon(Bitmap bitmap) {
            Resources res = getResources();
            int height = (int) res.getDimension(android.R.dimen.notification_large_icon_height);
            int width = (int) res.getDimension(android.R.dimen.notification_large_icon_width);
            return Bitmap.createScaledBitmap(bitmap, width, height, false);
        }

        @Override
        public void onBitmapFailed(Drawable errorDrawable) {
        }

        @Override
        public void onPrepareLoad(Drawable placeHolderDrawable) {
            if (placeHolderDrawable != null) {
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single_image);
        ImageView imageView = (ImageView) findViewById(R.id.image);
        url = getIntent().getStringExtra("URL");
        name = getIntent().getStringExtra("ID");
        Picasso.with(SingleImageActivity.this).load(url).placeholder(R.drawable.loading).into(imageView);

        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        final ImageButton save = (ImageButton) findViewById(R.id.save);

        Picasso.with(this)
                .load(url)
                .into(imageView, new Callback.EmptyCallback() {
                    @Override
                    public void onSuccess() {
                        progressBar.setVisibility(View.GONE);
                        save.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onError() {
                        progressBar.setVisibility(View.VISIBLE);
                    }
                });
    }

    public void downloadImage(View v) {
        Picasso.with(SingleImageActivity.this).load(url).into(target);
    }
}