package com.students.nearbyflickr.flickr;

import android.graphics.Bitmap;

public class FlickrImage {
    private static final int PHOTO_THUMB = 1;
    private static final int PHOTO_LARGE = 2;
    private static final int PHOTO_SQUARE = 3;
    private String id;
    private int position;
    private String thumbURL;
    private String squareURL;
    private Bitmap thumb;
    private Bitmap photo;
    private double latitude;
    private double longitude;
    private String largeURL;
    private String owner;
    private String secret;
    private String server;
    private String farm;

    public FlickrImage(String id, String thumbURL, String largeURL, String owner, String secret, String server, String farm) {
        super();
        this.id = id;
        this.owner = owner;
        this.secret = secret;
        this.server = server;
        this.farm = farm;
    }

    public FlickrImage() {
    }

    public FlickrImage(String id, double latitude, double longitude) {
        this.id = id;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public FlickrImage(String id, String owner, String secret, String server, String farm) {
        super();
        this.id = id;
        this.owner = owner;
        this.secret = secret;
        this.server = server;
        this.farm = farm;
        setThumbURL(createPhotoURL(PHOTO_THUMB, this));
        setLargeURL(createPhotoURL(PHOTO_LARGE, this));
        setSquareURL(createPhotoURL(PHOTO_SQUARE, this));
    }

    public String getThumbURL() {
        return thumbURL;
    }

    public void setThumbURL(String thumbURL) {
        this.thumbURL = thumbURL;
    }

    public String getLargeURL() {
        return largeURL;
    }

    public void setLargeURL(String largeURL) {
        this.largeURL = largeURL;
    }


    public String getSquareURL() {
        return squareURL;
    }

    public void setSquareURL(String squareURL) {
        this.squareURL = squareURL;
    }

    @Override
    public String toString() {
        return "FlickrImage [id=" + id + ", thumbURL=" + thumbURL + ", largeURL=" + largeURL + ", owner=" + owner + ", secret=" + secret + ", server=" + server + ", farm="
                + farm + "]";
    }

    private String createPhotoURL(int photoType, FlickrImage imgCon) {
        String tmp = null;
        tmp = "https://farm" + imgCon.farm + ".staticflickr.com/" + imgCon.server + "/" + imgCon.id + "_" + imgCon.secret;// +".jpg";
        switch (photoType) {
            case PHOTO_THUMB:
                tmp += "_m";
                break;
            case PHOTO_LARGE:
                tmp += "_b";
                break;
            case PHOTO_SQUARE:
                tmp += "_s";
                break;
        }
        tmp += ".jpg";
        return tmp;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public Bitmap getThumb() {
        return thumb;
    }

    public void setThumb(Bitmap thumb) {
        this.thumb = thumb;
    }

    public Bitmap getPhoto() {
        return photo;
    }

    public void setPhoto(Bitmap photo) {
        this.photo = photo;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getSecret() {
        return secret;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }

    public String getServer() {
        return server;
    }

    public void setServer(String server) {
        this.server = server;
    }

    public String getFarm() {
        return farm;
    }

    public void setFarm(String farm) {
        this.farm = farm;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }
}
