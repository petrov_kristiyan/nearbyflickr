package com.students.nearbyflickr.flickr;

import android.content.Context;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class FlickrManager {

    // String to create Flickr API urls
    private static final String FLICKR_BASE_URL = "https://api.flickr.com/services/rest/?method=";
    private static final String FLICKR_PHOTOS_SEARCH_STRING = "flickr.photos.search";
    private static final String FLICKR_GEO_GET_LOCATION = "flickr.photos.geo.getLocation";
    public static  int NUMBER_OF_PHOTOS = 30;
    //You can set here your API_KEY
    private static final String APIKEY_SEARCH_STRING = "&api_key=4dbf0efbf430bb10061ceeaf3f50400f";
    private static final String LAT_STRING = "&lat=";
    private static final String LON_STRING = "&lon=";
    private static final String ACC_STRING = "&accuracy=";
    public static String ACC_VALUE = "11";
    private static final String FORMAT_STRING = "&format=json&nojsoncallback=1";
    private static final String PHOTO_ID_STRING = "&photo_id=";


    /**
     * @param lat
     * @param lon
     * @return
     */
    public static String createURL(double lat, double lon, int page) {
        String url = FLICKR_BASE_URL + FLICKR_PHOTOS_SEARCH_STRING + APIKEY_SEARCH_STRING + LAT_STRING + lat + LON_STRING + lon + ACC_STRING +ACC_VALUE+ FORMAT_STRING + "&per_page=" + NUMBER_OF_PHOTOS + "&media=photos&sort=date-posted-desc&page=" + page;
        return url;
    }
    //used in Maps
    public static String createURL_Geo_Location(String id) {
        String url = FLICKR_BASE_URL + FLICKR_GEO_GET_LOCATION + APIKEY_SEARCH_STRING +FORMAT_STRING+PHOTO_ID_STRING + id;
        return url;
    }

    public static ArrayList<FlickrImage> searchImageByLocation( String json) {
        ArrayList<FlickrImage> tmp = new ArrayList<FlickrImage>();
        JSONObject root;
        try {
            root = new JSONObject(json);
            JSONObject photo = root.getJSONObject("photos");
            JSONArray imageJSONArray = photo.getJSONArray("photo");
            for (int index = 0; index < imageJSONArray.length(); index++) {
                JSONObject item = imageJSONArray.getJSONObject(index);
                FlickrImage img = new FlickrImage(item.getString("id"), item.getString("owner"), item.getString("secret"), item.getString("server"), item.getString("farm"));
                img.setPosition(index);
                tmp.add(img);
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return tmp;
    }

    public static FlickrImage getImagesLocation(String json) {
        FlickrImage flickrImage = new FlickrImage();
        JSONObject root;
        try {
            root = new JSONObject(json);
            JSONObject photo = root.getJSONObject("photo");
            JSONObject location = photo.getJSONObject("location");
             flickrImage = new FlickrImage(photo.getString("id"),location.getDouble("latitude"),location.getDouble("longitude"));
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return flickrImage;
    }
}