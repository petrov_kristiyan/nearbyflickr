package com.students.nearbyflickr;

import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.location.LocationClient;
import com.google.android.gms.location.LocationRequest;
import com.students.nearbyflickr.adapter.ViewPagerAdapter;
import com.students.nearbyflickr.flickr.FlickrManager;
import com.students.nearbyflickr.fragment.GalleryFragment;
import com.students.nearbyflickr.intentService.LocationService;

import java.lang.reflect.Method;


public class Main_Activity extends FragmentActivity implements GooglePlayServicesClient.ConnectionCallbacks, GooglePlayServicesClient.OnConnectionFailedListener, GalleryFragment.JSON {

    private static final String TAG = "Main_Activity";

    private static final int RESULT_SETTINGS = 1;
    private static final int MINUTE_IN_MILLISECONDS = 60000;
    private static final int ONE_HOUR = 60 * MINUTE_IN_MILLISECONDS; //max 1 hour interval before update
    private int interval = 5 * MINUTE_IN_MILLISECONDS;  // default is 5 minutes
    private static int count = 0;    //counter used to check that at least 1 image was loaded before letting user change page
    private static ViewPagerAdapter viewPagerAdapter;
    private static CustomViewPager customViewPager;
    private static int page = 1;        //current page, used to get right Json and keep track of current page, ignoring tutorial
    private LocationClient locationclient;
    private LocationRequest locationrequest;
    private Intent mIntentService;
    private PendingIntent mPendingIntent;
    private double latitude = 0;
    private double longitude = 0;
    private BroadcastReceiver location = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            latitude = intent.getDoubleExtra("LATITUDE", latitude);
            longitude = intent.getDoubleExtra("LONGITUDE", longitude);
            progressMessage.setVisibility(View.GONE);   //hide Message
            if (viewPagerAdapter != null) {
                viewPagerAdapter.notifyDataSetChanged();    //resets current page with new Data
            } else {//in case the
                viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager(), locationclient, page);
                customViewPager = (CustomViewPager) findViewById(R.id.pager);
                customViewPager.setAdapter(viewPagerAdapter);
            }
        }
    };
    private TextView progressMessage;
    private int locationPriority;
    private String oldJson; //keep old Json in memory to pass to Map activity
    private String json;    //newest json

    public static void setPagingEnabled(boolean enable) {//enables sliding to next or previous page
        customViewPager.setPagingEnabled(enable);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mIntentService = new Intent(this, LocationService.class);
        mPendingIntent = PendingIntent.getService(this, 1, mIntentService, 0);
        progressMessage = (TextView) findViewById(R.id.messageProgress);
        int resp = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (resp == ConnectionResult.SUCCESS) {
            locationclient = new LocationClient(this, this, this);
            locationclient.connect();
        } else {
            Toast.makeText(this, "Google Play Service Error " + resp, Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main_, menu);
        return true;
    }

    @Override
    public boolean onMenuOpened(int featureId, Menu menu) { //shows icon on overflow menu
        if (featureId == Window.FEATURE_ACTION_BAR && menu != null) {
            if (menu.getClass().getSimpleName().equals("MenuBuilder")) {
                try {
                    Method m = menu.getClass().getDeclaredMethod("setOptionalIconsVisible", Boolean.TYPE);
                    m.setAccessible(true);
                    m.invoke(menu, true);
                } catch (NoSuchMethodException e) { //...
                } catch (Exception e) { // ...
                }
            }
        }
        return super.onMenuOpened(featureId, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_settings:
                Intent i = new Intent(this, SettingsActivity.class);
                startActivityForResult(i, RESULT_SETTINGS);
                break;
            case R.id.update:
                if (page > 0) {
                    if (viewPagerAdapter != null) {
                        viewPagerAdapter.notifyDataSetChanged();
                    }
                }
                return true;
            case R.id.map:
                Intent intent = new Intent(this, MapsActivity.class);
                intent.putExtra("JSON", oldJson);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
        return true;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (locationclient != null)
            locationclient.disconnect();
    }

    @Override
    public void onConnected(Bundle connectionHint) {
        Log.i(TAG, "onConnected");
        Location loc = locationclient.getLastLocation();
        locationrequest = LocationRequest.create();
        locationrequest.setInterval(ONE_HOUR);
        if (loc != null) {
            latitude = loc.getLatitude();
            longitude = loc.getLongitude();

            viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager(), locationclient, page);
            customViewPager = (CustomViewPager) findViewById(R.id.pager);
            customViewPager.setAdapter(viewPagerAdapter);
            showUserSettings();

            customViewPager.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
                @Override
                public void onPageSelected(int i) {
                    page = i;
                    oldJson = json;
                    viewPagerAdapter.setPage(page);//used to update page value in adapter to be used when called getItemPosition() from notifyDataSetChanged()
                    if (page > 0) {
                        setPagingEnabled(false);
                    }
                }
            });
        } else {
            checkGPS();

        }

    }

    public void checkGPS() {    //check which services are enabled
        LocationManager lm = null;
        String message = "";
        boolean gps_enabled = false;
        boolean network_enabled = false;
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);

        // Setting Dialog Title
        alertDialog.setTitle("GeoLocation Settings");

        // Setting Dialog Message

        if (lm == null)
            lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        try {
            gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
        } catch (Exception ex) {
        }
        try {
            network_enabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        } catch (Exception ex) {
        }


        // On pressing Settings button
        alertDialog.setPositiveButton("Settings", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(intent);
                progressMessage.setVisibility(View.VISIBLE);
            }
        });

        // on pressing cancel button
        final boolean finalGps_enabled = gps_enabled;
        final boolean finalNetwork_enabled = network_enabled;
        alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                if (!finalGps_enabled && !finalNetwork_enabled)
                    finish();   //if neither is enabled and cancel was chosen, close application
                else {
                    progressMessage.setVisibility(View.VISIBLE);
                }

            }
        });

        if (!gps_enabled && !network_enabled) {
            message = "GeoLocation is not enabled. Do you want to go to settings menu?";
            alertDialog.setMessage(message);
            // Showing Alert Message
            alertDialog.show();
        } else {
            if (gps_enabled && !network_enabled) {
                message = "Only GPS is enabled, it may take a while until the application is able to get your location...";
                alertDialog.setMessage(message);
                // Showing Alert Message
                alertDialog.show();
            }
        }


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case RESULT_SETTINGS:
                SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(this);
                if (resultCode == 1) {
                    if (data.getBooleanExtra("prefPhotoNumber", false))
                        prefPhotoNumber(sharedPrefs);
                    if (data.getBooleanExtra("prefAccuracy", false))
                        prefAccuracy(sharedPrefs);
                    if (data.getBooleanExtra("prefLocationPriority", false))
                        prefLocationPriority(sharedPrefs);
                    if (data.getBooleanExtra("prefUpdateFrequency", false))
                        prefUpdateFrequency(sharedPrefs);
                }

                break;
        }

    }

    private void prefPhotoNumber(SharedPreferences sharedPrefs) {
        if (viewPagerAdapter != null) {
            String photoNumberString = sharedPrefs.getString("prefPhotoNumber", "30");
            FlickrManager.NUMBER_OF_PHOTOS = Integer.parseInt(photoNumberString.toString()); // get number of photos from preferences
            viewPagerAdapter.notifyDataSetChanged();
        }
    }

    private void prefAccuracy(SharedPreferences sharedPrefs) {
        if (viewPagerAdapter != null) {
            String accuracy = sharedPrefs.getString("prefAccuracy", "11");
            FlickrManager.ACC_VALUE = accuracy;
            viewPagerAdapter.notifyDataSetChanged();
        }
    }

    private void prefLocationPriority(SharedPreferences sharedPrefs) {
        locationPriority = Integer.parseInt(sharedPrefs.getString("prefLocationPriority", "102"));//102 is balanced priority
        locationrequest.setPriority(locationPriority);
        locationclient.requestLocationUpdates(locationrequest, mPendingIntent);
    }

    private void prefUpdateFrequency(SharedPreferences sharedPrefs) {
        interval = Integer.parseInt(sharedPrefs.getString("prefUpdateFrequency", "5")); // get minutes from preferences
        interval = interval * MINUTE_IN_MILLISECONDS;
        if (interval == 0) {
            locationclient.removeLocationUpdates(mPendingIntent);
        } else {
            locationrequest.setFastestInterval(interval);
            locationclient.requestLocationUpdates(locationrequest, mPendingIntent);
        }
    }

    private void showUserSettings() {
        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(this);
        prefPhotoNumber(sharedPrefs);
        prefAccuracy(sharedPrefs);
        prefLocationPriority(sharedPrefs);
        prefUpdateFrequency(sharedPrefs);

    }

    @Override
    public void onDisconnected() {
        Log.i(TAG, "onDisconnected");
    }

    @Override
    public void onConnectionFailed(ConnectionResult result) {
        Log.i(TAG, "onConnectionFailed");

    }

    @Override
    protected void onResume() {
        super.onResume();
        if (locationclient == null)
            checkGPS();
        registerReceiver(location, new IntentFilter(LocationService.LOCATION));
        if (locationclient != null && locationclient.isConnected())
            locationclient.requestLocationUpdates(locationrequest, mPendingIntent);
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(location);
        if (locationclient != null && locationclient.isConnected())
            locationclient.removeLocationUpdates(mPendingIntent);
    }
    @Override
    public void setJson(String json1) {
        json = json1;
    }
    public static int getCount(){
        return count;
    }
    public static void setCount(int countn){
        count = countn;
    }
}
