package com.students.nearbyflickr;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.ImageView;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;
import com.students.nearbyflickr.R;
import com.students.nearbyflickr.flickr.FlickrImage;
import com.students.nearbyflickr.flickr.FlickrManager;
import com.students.nearbyflickr.intentService.Download_JSON;

import java.util.ArrayList;

public class MapsActivity extends FragmentActivity {

    private Marker markerPoint;
    private GoogleMap mMap; // Might be null if Google Play services APK is not available.
    private ArrayList<FlickrImage> imageList;
    private FlickrImage flickrImage;
    private View marker;
    private BroadcastReceiver photoLocation = new BroadcastReceiver() {
        @Override
        public void onReceive(final Context context, Intent intent) {
            try {
                Bundle bundle = intent.getExtras();
                if (bundle != null) {
                    String json = bundle.getString("JSON");
                    flickrImage = FlickrManager.getImagesLocation(json);
                    for (FlickrImage img : imageList) {
                        if (img.getId().equals(flickrImage.getId())) {
                            markerPoint = mMap.addMarker(new MarkerOptions()
                                    .position(new LatLng(flickrImage.getLatitude(), flickrImage.getLongitude())));//default marker
                           //get images from cache and draw them on map
                            Picasso.with(context).load(img.getSquareURL()).noFade().into(new Target() {
                                @Override
                                public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                                    imageView.setImageBitmap(bitmap);
                                    markerPoint.setIcon(BitmapDescriptorFactory.fromBitmap(shapeBitmap(marker)));//change marker icon with custom icon with image
                                }

                                @Override
                                public void onBitmapFailed(Drawable errorDrawable) {

                                }

                                @Override
                                public void onPrepareLoad(Drawable placeHolderDrawable) {

                                }
                            });
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };
    private ImageView imageView;
    private String json;

    private Bitmap shapeBitmap( View view) {    //turns view into bitmap , used for the image  marker
        Bitmap bitmap = Bitmap.createBitmap(view.getMeasuredWidth(), view.getMeasuredHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        view.draw(canvas);
        return bitmap;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        json = getIntent().getStringExtra("JSON");
        if(json != null ) {
            marker = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.custom_marker, null);
            DisplayMetrics displayMetrics = new DisplayMetrics();
            getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
            marker.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
            marker.measure(displayMetrics.widthPixels, displayMetrics.heightPixels);
            marker.layout(0, 0, displayMetrics.widthPixels, displayMetrics.heightPixels);
            marker.buildDrawingCache();
            imageView = (ImageView) marker.findViewById(R.id.photo);
            setUpMapIfNeeded();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        setUpMapIfNeeded();
        registerReceiver(photoLocation, new IntentFilter(Download_JSON.NOTIFICATION_PHOTO_GEO_LOCATION));

    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(photoLocation);
    }

    private void setUpMapIfNeeded() {
        // Do a null check to confirm that we have not already instantiated the map.
        if (mMap == null) {
            // Try to obtain the map from the SupportMapFragment.
            mMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map)).getMap();
            // Check if we were successful in obtaining the map.
            if (mMap != null) {
                mMap.setMyLocationEnabled(true);
                try {
                    if (json != null) {
                        imageList = FlickrManager.searchImageByLocation(json);
                        for (FlickrImage img : imageList) {
                            String url = FlickrManager.createURL_Geo_Location(img.getId());
                            Intent intent_Photo_Geo_Location = new Intent(this, Download_JSON.class);
                            intent_Photo_Geo_Location.putExtra("IMG_GEO_LOCATION_URL", url);
                            startService(intent_Photo_Geo_Location);
                        }
                    }
                } catch (Exception e) {
                    Log.e("location", e.toString());
                }
            }
        }
    }
}
