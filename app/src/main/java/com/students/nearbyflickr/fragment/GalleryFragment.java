package com.students.nearbyflickr.fragment;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.RelativeLayout;

import com.students.nearbyflickr.R;
import com.students.nearbyflickr.adapter.GridViewAdapter;
import com.students.nearbyflickr.flickr.FlickrImage;
import com.students.nearbyflickr.flickr.FlickrManager;
import com.students.nearbyflickr.intentService.Download_JSON;

import java.util.ArrayList;


public class GalleryFragment extends Fragment{
    public static interface JSON {
        public abstract void setJson(String json);
    }
    private JSON mListener;

    private  View view;
    private  int page = 1;
    private  String json;
    private  ArrayList<FlickrImage> imageList;
    private  GridView gridView;
    private  GridViewAdapter customGridAdapter;
    private BroadcastReceiver gallery = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Bundle bundle = intent.getExtras();
            if (bundle != null) {
                if(bundle.getInt("PAGE")==page) {
                    json = bundle.getString("JSON");
                    mListener.setJson(json);// update anyway the json even if is not for this page, maps activity needs an updated json
                    imageList = FlickrManager.searchImageByLocation(json);
                    customGridAdapter = new GridViewAdapter(context, R.layout.grid_element, imageList);
                    gridView.setAdapter(customGridAdapter);
                }
            }
        }
    };

    /**
     * Transform Px to Dp
     * @param px
     * @return
     */
    public int getDPfromPX(int px){
       return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, px, getResources().getDisplayMetrics());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.gallery_fragment, container, false);
        String url = getArguments().getString("URL");
        //set GridView
        gridView = new GridView(getActivity());
        gridView.setGravity(Gravity.CENTER);
        gridView.setNumColumns(GridView.AUTO_FIT);
        gridView.setStretchMode(GridView.STRETCH_COLUMN_WIDTH);
        gridView.setVerticalSpacing(getDPfromPX(5));
        gridView.setColumnWidth(getDPfromPX(150));
        gridView.setDrawSelectorOnTop(true);
        gridView.setLayoutParams(new GridView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.WRAP_CONTENT));
        RelativeLayout relativeLayout = (RelativeLayout)view.findViewById(R.id.galleryLayout);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.FILL_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        layoutParams.setMargins(getDPfromPX(5), getDPfromPX(5), getDPfromPX(5), getDPfromPX(5));
        relativeLayout.addView(gridView,layoutParams);
        //call intentservice to download json
        page = getArguments().getInt("PAGE");
        Intent intent = new Intent(getActivity(), Download_JSON.class);
        intent.putExtra(Download_JSON.URL_PHOTOS_GALLERY, url);
        intent.putExtra("PAGE",page);
        getActivity().startService(intent);

        return view;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            this.mListener = (JSON)getActivity();
        }
        catch (final ClassCastException e) {
            throw new ClassCastException(getActivity().toString() + " must implement JSON");
        }

    }

    @Override
    public void onResume() {
        super.onResume();
        getActivity().registerReceiver(gallery, new IntentFilter(Download_JSON.NOTIFICATION_GALLERY));
    }

    @Override
    public void onPause() {
        super.onPause();
        getActivity().unregisterReceiver(gallery);
    }


}

